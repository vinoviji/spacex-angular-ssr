# spacex-angular-ssr

Angular server side rendering with angular universal to list and filter spacex missions.

Features
---------

Angular Server Side Renedering
Css Grid and Flex
Responsive Web Design

Screenshots
-----------

Desktop View

https://www.awesomescreenshot.com/image/6057387?key=e05d9ed1a14b8c10b23be204b80212a1

Mobile View

https://www.awesomescreenshot.com/image/6057275?key=610949f531044401ed001867c3638caa

Tablet View

https://www.awesomescreenshot.com/image/6057472?key=6c19062c0b56bd0d8fa0d2163da7e5d2

Commands to Run
---------------

Dev: npm run dev:ssr 

Prod: npm run build:ssr


